#!/bin/bash

echo "Running smartctl.sh"

DEVICE=$1

# Ensure smartctl is available and can be executed with sudo without a password
if ! which smartctl > /dev/null; then
    echo "smartctl is not installed. Install it with 'sudo apt-get install smartmontools'"
    exit 1
fi

# Running smartctl command to fetch serial directly using sudo
SERIAL=$(sudo smartctl -i /dev/$DEVICE | grep -i 'Serial Number:' | awk '{print $NF}')
if [ -z "$SERIAL" ]; then
    echo "Failed to retrieve serial number for device /dev/$DEVICE. Check if device exists and SMART is enabled."
    exit 1
fi

TIMESTAMP=$(date +%Y%m%d-%H%M%S)
FILENAME="${SERIAL}_${TIMESTAMP}.txt"


echo "Generating report for DISK $DEVICE - Serial: $SERIAL"

# Collect full smartctl data to verify the output
FULL_SMART_DATA=$(sudo smartctl -a /dev/$DEVICE)
echo "$FULL_SMART_DATA" > "report_${FILENAME}"

# Calculate the script's directory to find the .service file relative to it
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
BASE_DIR=$(dirname "$SCRIPT_DIR")  # This is the parent directory of Utils, likely the workspace root
PYTHON_FILE_PATH="$BASE_DIR/Utils/interpret_report.py"

python3 $PYTHON_FILE_PATH report_${FILENAME}

echo "Saved to report_${FILENAME}"
