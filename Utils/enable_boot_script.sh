#!/bin/bash
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
SERVICE_FILE_PATH="$SCRIPT_DIR/../Boot/ssd-testing.service"

sudo cp $SERVICE_FILE_PATH /etc/systemd/system/ssd-testing.service

sudo chmod +x /etc/systemd/system/ssd-testing.service

# Enable and start the service
echo "Enabling"
sudo systemctl daemon-reload
sudo systemctl enable ssd-testing.service

