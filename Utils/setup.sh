#!/bin/bash

# Determine the current user and their home directory
CURRENT_USER=$(whoami)
WORKING_DIR=$(eval echo ~$CURRENT_USER)


# Calculate the script's directory to find the .service file relative to it
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
BASE_DIR=$(dirname "$SCRIPT_DIR")  # This is the parent directory of Utils, likely the workspace root
SERVICE_FILE_PATH="$BASE_DIR/Boot/ssd-testing.service"

# Update system and install required packages
sudo adduser $CURRENT_USER dialout
sudo apt update -y
sudo apt install -y smartmontools openssh-* fio python3-pip 

# python dep
pip3 install pyserial pandas

# Check if the .service file exists
if [ -f "$SERVICE_FILE_PATH" ]; then
    # Update WorkingDirectory and User in the .service file
    sudo sed -i "s|WorkingDirectory=.*|WorkingDirectory=$WORKING_DIR/|" $SERVICE_FILE_PATH
    sudo sed -i "s|User=.*|User=$CURRENT_USER|" $SERVICE_FILE_PATH
    sudo sed -i "s|ExecStart=.*|ExecStart=$BASE_DIR/Boot/main.py|" $SERVICE_FILE_PATH  # Correct the path

    echo "Updated .service file."
else
    echo "Error: Service file not found at $SERVICE_FILE_PATH"
fi
