#!/usr/bin/env python3
import serial
import os
import time
import subprocess
import signal
import configparser
import logging
from datetime import datetime

def log_error(message):
    """Function to log an error only when called with a message."""
    if message:  # Check to make sure a message is actually provided
        error_time = datetime.now()
        log_filename = f'error_{error_time.strftime("%Y%m%d_%H%M%S")}.log'
        log_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), log_filename)
        
        # Create a logger instance for the error
        logger = logging.getLogger('ErrorLogger')
        logger.setLevel(logging.ERROR)
        
        # Set up the logging to file with the timestamped filename
        file_handler = logging.FileHandler(log_file_path, mode='w')
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        
        # Add the handler to the logger
        logger.addHandler(file_handler)
        
        # Log the error
        logger.error(message)
        
        # Remove the handler to prevent multiple log entries or file creations
        logger.removeHandler(file_handler)

class RS232_Pin():
    """ Class to control the RS232 RTS pin """
    def __init__(self, port:str, baudrate:int=9600):
        self.serial_controller = serial.Serial(port, baudrate)

    def toggle_pin_high(self):
        self.serial_controller.setRTS(1)

    def toggle_pin_low(self):
        self.serial_controller.setRTS(0)


class File_System_Check():
    """ Class to check the file system integrity of the system """

    def __init__(self,
        SSD1_file_system_path: str,
        SSD2_file_system_path: str):

        try:
            self.SSD1_file_system_path = SSD1_file_system_path
            self.SSD2_file_system_path = SSD2_file_system_path
            if not os.path.exists(self.SSD1_file_system_path):
                raise FileNotFoundError("SSD1 file system path does not exist: " + self.SSD1_file_system_path)

            if not os.path.exists(self.SSD2_file_system_path):
                raise FileNotFoundError("SSD2 file system path does not exist: " + self.SSD2_file_system_path)
                                
        except Exception as e:
            raise(e)

    def run_file_system_test(self):
        try:
            self._generate_new_checksum()

            command_01 = f"cmp {self.SSD1_file_system_path}/checksum_master.txt {self.SSD1_file_system_path}/checksum_new.txt"
            result_01 = subprocess.run(command_01, shell=True, encoding='utf-8')

            command_02 = f"cmp {self.SSD2_file_system_path}/checksum_master.txt {self.SSD2_file_system_path}/checksum_new.txt"
            result_02 = subprocess.run(command_02, shell=True, encoding='utf-8')


            if result_01.returncode == 0 and result_02.returncode == 0:
                print("File system test passed")

            elif result_01 != 0:
                raise ValueError(f"{self.SSD1_file_system_path} file test failed")

            elif result_02 != 0:
                raise ValueError(f"{self.SSD2_file_system_path} file test failed")

            else:
                raise ValueError("File system test failed")
        
        except Exception as e:
            raise e

    def _generate_new_checksum(self):
        try:
            command_01 = f"md5sum {self.SSD1_file_system_path}/cantrbry//* > {self.SSD1_file_system_path}/checksum_new.txt"
            subprocess.run(command_01, shell=True, encoding='utf-8')
            output_file = f"{self.SSD1_file_system_path}/checksum_new.txt"
            self._filter_md5sum(output_file)

            command_02 = f"md5sum {self.SSD2_file_system_path}/cantrbry//* > {self.SSD2_file_system_path}/checksum_new.txt"
            subprocess.run(command_02, shell=True, encoding='utf-8') 
            output_file = f"{self.SSD2_file_system_path}/checksum_new.txt"
            self._filter_md5sum(output_file)

        except Exception as e:
            raise e
            

    def _filter_md5sum(self, md5_output_file_path: str):
        try:
            # Go through file and remove everything from first space for each line
            md5_list = []
            with open(md5_output_file_path, 'r') as file:
                for line in file:
                    md5_list.append(line)
        
            with open(md5_output_file_path, 'w') as file:
                # Write the md5 strings to the file but remove the last blank line
                for md5_string in md5_list[:-1]:
                    md5_string = md5_string.split(' ')[0]
                    file.write(md5_string + '\n')

                last_md5_string = md5_list[-1].split(' ')[0]
                file.write(last_md5_string)
        except Exception as e:
            raise e

    


class Disk_Load_Simulator:
    """ Class to create a FIO load on a disk space """
    def __init__(self, 
        fio_job_file_path: str, 
        SSD1_file_system_path: str, 
        SSD2_file_system_path: str,
        loop_stop_time_minutes: float):

        try:
            self.fio_job_file_path = fio_job_file_path

            self.SSD1_file_system_path = SSD1_file_system_path
            if not os.path.exists(self.SSD1_file_system_path):
                raise FileNotFoundError("SSD1 system path does not exist: " + self.SSD1_file_system_path)

            self.SSD1_fio_output_path = os.path.join(self.SSD1_file_system_path, 'fio_output')
            if not os.path.exists(self.SSD1_fio_output_path):
                raise FileNotFoundError("Create the /fio_output directory for SSD1 at: " + self.SSD1_fio_output_path)

            self.SSD2_file_system_path = SSD2_file_system_path
            if not os.path.exists(self.SSD2_file_system_path):
                raise FileNotFoundError("SSD2 path does not exist: " + self.SSD2_file_system_path)

            self.SSD2_fio_output_path = os.path.join(self.SSD2_file_system_path, 'fio_output')
            if not os.path.exists(self.SSD2_fio_output_path):
                raise FileNotFoundError("Create the /fio_output directory for SSD2 at: " + self.SSD2_fio_output_path)

            if loop_stop_time_minutes <= 0:
                raise ValueError("Loop stop time must be greater than 0")
            
            self.loop_stop_time_seconds = loop_stop_time_minutes * 60 
            self.start_time = time.time()
            self.continue_running_flag = True
            
            self._update_job_file()
            
            # Setting up signal handler for graceful interruption
            signal.signal(signal.SIGINT, self.signal_handler)
        
        except Exception as e:
            raise(e)


    def run_load_sim_loop(self):
            while self.continue_running_flag:
                # Start the FIO process using subprocess.Popen
                fio_process = subprocess.Popen(['fio', self.fio_job_file_path])

                try:
                    # Check if the process is still running and the timeout has not been reached
                    while fio_process.poll() is None:
                        if (time.time() - self.start_time) >= self.loop_stop_time_seconds:
                            print("Overall time limit reached, stopping load simulation.")
                            fio_process.terminate()
                            fio_process.wait()

                            raise ValueError("Time limit reached")
                    
                    # If the FIO job completes and the total time is still not up, restart it
                    if (time.time() - self.start_time) < self.loop_stop_time_seconds:
                        print("FIO job completed, restarting...")
                        continue

                    # If the process is still running by the time limit, terminate it
                    if fio_process.poll() is None:
                        print("Timeout reached - terminating FIO process.")
                        fio_process.terminate()
                        fio_process.wait()

                except Exception as e:
                    raise e


    def signal_handler(self, signum, frame):
        print("Termination signal received, stopping...")
        self.continue_running_flag = False

    def _update_job_file(self):
        config = configparser.ConfigParser()
        config.read(self.fio_job_file_path)

        if 'global' not in config:
            config['global'] = {}
        
        config['global']['directory'] = f"{self.SSD1_fio_output_path}:{self.SSD2_fio_output_path}"

        with open(self.fio_job_file_path, 'w') as configfile:
            config.write(configfile, space_around_delimiters=False)


if __name__ == ("__main__"):
    try:
        import time
        import json 
        import os


        config_path = os.path.join(os.path.dirname(__file__), '../config.json')
        with open(config_path) as config_file:
            config = json.load(config_file)

        # Set pin high to indicate that the system is ok
        feedback_pin = RS232_Pin(config["port"])
        print("Toggling high")
        feedback_pin.toggle_pin_high()

        file_system_check = File_System_Check(
            SSD1_file_system_path=config["SSD1_file_system_path"],
            SSD2_file_system_path=config["SSD2_file_system_path"]
            )

        file_system_check.run_file_system_test()

        load_testing = Disk_Load_Simulator(
            fio_job_file_path=config["job_file_path"], 
            SSD1_file_system_path=config["SSD1_file_system_path"],
            SSD2_file_system_path=config["SSD2_file_system_path"],
            loop_stop_time_minutes=config["fio_loop_stop_time_minutes"]
            )    

        load_testing.run_load_sim_loop()

    except Exception as e:
        print("Error:", e)
        log_error(str(e))

    finally:
        print("Exiting, toggling low")
        feedback_pin.toggle_pin_low()
