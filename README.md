# SSD Testing Setups

This repo contains the setup files for the SSD testing environment. The test is conducted on atleast 1 IPC with a SSD. The test is conducted by creating a disk load on the SSD and then running the IPC test.

## Project Structure

### [Boot](Boot)
This directory contains the script and instructions for setting up the SSD testing environment.

### [FileSystem](FileSystem)
This directory should be placed in the SSD bulk storage. For example in `/mnt/ata-SQF-S25M8-512G-SAE_AO2240110197000132-part1`.

### [Utils](Utils)
This directory contains the utility scripts for the SSD testing environment.

## Setup

### Disk Setup
1. Setup a bootable USB drive with Ubuntu 18.04 image
2. Boot into USB drive
3. Partion the 2 SSDs as follows:
```
SSD1: Linux filesystem - 60 GB, Remaining GB as EXT4
SSD2: All GB as EXT4
```
4. On next boot, go to `Disks` application and do the following:
- On unused EXT4 disk space, select `Edit Mount Options...` from the Settings icon
- Toggle `User Session Defaults` Off
- Check the boxes: `Mount at system startup`, and `Show in User Interface`.
- Ensure `Mount Point` field shows the device mounted to `/mnt/` followed by device UUID
- Update permissions to write to the newly mounted devices. May require reboot, there's a command to mount but I forgot. So reboot and do, `sudo chmod ugo+wx /mnt/b388cc68-3efa-4075-a06c-d4eacf63397b/`

### IPC Testing Setup

#### Permissions

1. Go to repo home directory `cd ssd-testing-ipc-setup`
2. Give permission to execute the utility scripts with `chmod +x /Utils/*`. `sudo` may be required. 
3. Execute the `./Utils/setup` script to install the required dependencies and configure environment variables.

### File System
1. Copy `FileSystem` to each of the SSDs to be tested.
**Warning:** Ensure this is in the seperate partition from your OS as the program will consume a large amount of disk space and could lock up your system preventing it from booting.
2. Create a `fio_output` directory inside of the new File System locations.

### Configuration Files
1. Update your `[job_file.fio](\Utils\job_file.fio)` with the correct paths to the disks.
2. Update the `[config.json](config.json)` with the correct paths. 
`port` - refers to the IPCs 232 port which will be used for feedback signaling
`job_file_path` - The location of the FIO jobfile. In `[Utils](\Utils)` by default.
`SSDX_file_system_path` - Path to either SSD1 or SSD2 file system location from previous step
`fio_loop_stop_timer_minutes` - A safety timer in case power cylcing mechanism fails. Will stop SSD testing operations.

### Boot Script
1. The boot script service can be enabled by executing the `./Utils/enable_boot_script` script. This will create the service that will run on boot.

To view any errors run `systemctl status ssd-testing.service`.
You are now ready to start testing the SSDs!


## Testing
In the [Setup](#Setup) section, you have setup all the paths and required services for the IPC to do th file check and disk load portions of the test. The remainder of this section explains what must now be done to get the required data for analysis.

The control and power cycling portion of this test is handled by a raspberry pi. The repository for the raspberry pi can be found [here](https://gitlab.com/juancruzplusone/rpi-interface).

The schematic for the current test rig can be seen below:

![schematic](SSD TESTING_1.jpg)


### SMART Analysis
To gauge disk health we use the SMART testing tool. You will need to conduct the test before and after your target power cycles.
1. Run a SMART test, `sudo smartctl --test=long /dev/$DEVICE` where `$DEVICE` is the SSD you are testing (Such as `sda, sdb` etc.). Do this for both SSDs to be tested.
2. When the test completes you can output a report as a text file using the following utility script: `./Utils/smartctl.sh $DEVICE` where `$DEVICE` is the SSD identifier. 
Example: `./Utils/smartctl.sh $DEVICE`

If any errors occur, they can be viewed with `systemctl status ssd-testing.service`
